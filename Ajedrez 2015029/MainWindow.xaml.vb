﻿Class MainWindow

    Private _matriz(0 To 7, 0 To 7) As Object
    Private _turno As String = "Blancas"
    Private _pawnBlackL As Boolean = True
    Private _pawnBlackR As Boolean = True
    Private _pawnWhiteL As Boolean = True
    Private _pawnWhiteR As Boolean = True
    Private _KingBlack As Boolean = True
    Private _KingWhite As Boolean = True

    Public Sub New()
        InitializeComponent()
        RestarPieces()
    End Sub

    Public Sub RestarPieces()

        InitializeComponent()

        Dim blackColor As New SolidColorBrush(Color.FromRgb(222, 227, 231))
        Dim whiteColor As New SolidColorBrush(Color.FromRgb(95, 107, 114))

        For rower As Integer = 0 To 7

            For columne As Integer = 0 To 7
                If columne Mod 2 = 0 Then
                    Dim whiteSquare As New Rectangle
                    whiteSquare.Fill = whiteColor
                    If (rower Mod 2) Then
                        Grid.SetRow(whiteSquare, rower)
                        Grid.SetColumn(whiteSquare, columne)
                    Else
                        Grid.SetRow(whiteSquare, rower)
                        Grid.SetColumn(whiteSquare, columne + 1)
                    End If
                    Tablero.Children.Add(whiteSquare)
                Else
                    Dim blackSquare2 As New Rectangle
                    blackSquare2.Fill = blackColor
                    If (rower Mod 2) Then
                        Grid.SetRow(blackSquare2, rower)
                        Grid.SetColumn(blackSquare2, columne)
                    Else
                        Grid.SetRow(blackSquare2, rower)
                        Grid.SetColumn(blackSquare2, columne - 1)
                    End If
                    Tablero.Children.Add(blackSquare2)

                End If
            Next
        Next
        PutPieces()
    End Sub

    Private Sub Window_SizeChanged(sender As Object, e As SizeChangedEventArgs)
        tagA.Width = (e.NewSize.Width \ 9)
        tagB.Width = (e.NewSize.Width \ 9)
        tagC.Width = (e.NewSize.Width \ 9)
        tagD.Width = (e.NewSize.Width \ 9)
        tagE.Width = (e.NewSize.Width \ 9)
        tagF.Width = (e.NewSize.Width \ 9)
        tagG.Width = (e.NewSize.Width \ 9)
        tagH.Width = (e.NewSize.Width \ 9)

        tag1.Height = (e.NewSize.Width \ 9)
        tag2.Height = (e.NewSize.Width \ 9)
        tag3.Height = (e.NewSize.Width \ 9)
        tag4.Height = (e.NewSize.Width \ 9)
        tag5.Height = (e.NewSize.Width \ 9)
        tag6.Height = (e.NewSize.Width \ 9)
        tag7.Height = (e.NewSize.Width \ 9)
        tag8.Height = (e.NewSize.Width \ 9)

    End Sub

    Dim _mouseX As Integer
    Dim _mouseY As Integer

    Private Sub Tablero_MouseMove(sender As Object, e As MouseEventArgs)
        Dim latter As String = ""
        Dim _mouseValue As String = e.GetPosition(sender).Y \ -75 + 8
        _mouseX = e.GetPosition(sender).X \ 75
        _mouseY = e.GetPosition(sender).Y \ 75
        Select Case _mouseX
            Case 0
                latter = "A"
            Case 1
                latter = "B"
            Case 2
                latter = "C"
            Case 3
                latter = "D"
            Case 4
                latter = "E"
            Case 5
                latter = "F"
            Case 6
                latter = "G"
            Case 7
                latter = "H"
        End Select
        Informacion.Text = "              Posición: " & latter & "" & _mouseValue &
            "             |             Turno: " & _turno & "              |            Tiempo: 14:53:00"
    End Sub

    Dim _blackColor As New SolidColorBrush(Colors.Yellow)
    Dim _whiteColor As New SolidColorBrush(Colors.Red)

    Public Sub PutPieces()

        For countPawn As Integer = 0 To 7
            Dim _whiteCircle As New PawnWhite
            Grid.SetColumn(_whiteCircle, countPawn)
            Grid.SetRow(_whiteCircle, 6)
            Tablero.Children.Add(_whiteCircle)
            _matriz(countPawn, 6) = _whiteCircle

            Dim _blackCircle As New PawnBlack
            Grid.SetColumn(_blackCircle, countPawn)
            Grid.SetRow(_blackCircle, 1)
            Tablero.Children.Add(_blackCircle)
            _matriz(countPawn, 1) = _blackCircle
        Next
        For countTower As Integer = 0 To 7 Step 7
            Dim _whiteCircle1 As New RookWhite
            Grid.SetColumn(_whiteCircle1, countTower)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(countTower, 7) = _whiteCircle1

            Dim _blackCircle1 As New RookBlack
            Grid.SetColumn(_blackCircle1, countTower)
            Grid.SetRow(_blackCircle1, 0)
            Tablero.Children.Add(_blackCircle1)
            _matriz(countTower, 0) = _blackCircle1
        Next
        For countTower As Integer = 1 To 6 Step 5
            Dim _whiteCircle1 As New KnightWhite
            Grid.SetColumn(_whiteCircle1, countTower)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(countTower, 7) = _whiteCircle1

            Dim _blackCircle1 As New BlackKnight
            Grid.SetColumn(_blackCircle1, countTower)
            Grid.SetRow(_blackCircle1, 0)
            Tablero.Children.Add(_blackCircle1)
            _matriz(countTower, 0) = _blackCircle1
        Next
        For countTower As Integer = 2 To 5 Step 3
            Dim _whiteCircle1 As New BishopWhite
            Grid.SetColumn(_whiteCircle1, countTower)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(countTower, 7) = _whiteCircle1

            Dim _blackCircle1 As New BishopBlack
            Grid.SetColumn(_blackCircle1, countTower)
            Grid.SetRow(_blackCircle1, 0)
            Tablero.Children.Add(_blackCircle1)
            _matriz(countTower, 0) = _blackCircle1
        Next
        For countTower As Integer = 4 To 4
            Dim _whiteCircle1 As New KingWhite
            Grid.SetColumn(_whiteCircle1, countTower)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(countTower, 7) = _whiteCircle1

            Dim _blackCircle1 As New KingBlack
            Grid.SetColumn(_blackCircle1, countTower)
            Grid.SetRow(_blackCircle1, 0)
            Tablero.Children.Add(_blackCircle1)
            _matriz(countTower, 0) = _blackCircle1
        Next
        For countTower As Integer = 3 To 3
            Dim _whiteCircle1 As New QueenWhite
            Grid.SetColumn(_whiteCircle1, 3)
            Grid.SetRow(_whiteCircle1, 7)
            Tablero.Children.Add(_whiteCircle1)
            _matriz(3, 7) = _whiteCircle1

            Dim _blackCircle1 As New QueenBlack
            Grid.SetColumn(_blackCircle1, 3)
            Grid.SetRow(_blackCircle1, 0)
            Tablero.Children.Add(_blackCircle1)
            _matriz(3, 0) = _blackCircle1
        Next

    End Sub

    Dim _alternador As Boolean = True
    Dim objeto As Object
    Dim objetoUp As Object
    Dim _newX As Integer
    Dim _newY As Integer
    Dim _value As String
    Dim _valid As Boolean = True
    Dim papu As Boolean

    Dim _selectionSquare As New SelectPiece

    Private Sub addSelection()
        Tablero.Children.Remove(_selectionSquare)
        Grid.SetRow(_selectionSquare, _mouseY)
        Grid.SetColumn(_selectionSquare, _mouseX)
        Tablero.Children.Add(_selectionSquare)
    End Sub

    Dim other As Integer

    Private Sub Jaque()
        If _matriz(_mouseX, _mouseY).ToString.Contains("PawnWhite") Then
            If Not IsNothing(_matriz(_mouseX + 1, _mouseY - 1)) And
                _matriz(_mouseX + 1, _mouseY - 1).ToString.Contains("KingBlack") Then
                MsgBox("Jaque Mate con tomate")
            ElseIf Not IsNothing(_matriz(_mouseX - 1, _mouseY - 1)) And
                _matriz(_mouseX - 1, _mouseY - 1).ToString.Contains("KingBlack") Then
                MsgBox("Jaque Mate con tomate")
            End If
        ElseIf _matriz(_mouseX, _mouseY).ToString.Contains("PawnBlack") Then
            If Not IsNothing(_matriz(_mouseX + 1, _mouseY + 1)) And
                _matriz(_mouseX + 1, _mouseY + 1).ToString.Contains("KingWhite") Then
                MsgBox("Jaque Mate con tomate")
            ElseIf Not IsNothing(_matriz(_mouseX - 1, _mouseY + 1)) And
                _matriz(_mouseX - 1, _mouseY + 1).ToString.Contains("KingWhite") Then
                MsgBox("Jaque Mate con tomate")
            End If
        ElseIf _matriz(_mouseX, _mouseY).ToString.Contains("Rook") Or _matriz(_mouseX, _mouseY).ToString.Contains("Queen") Then
            For count As Integer = _mouseY - 1 To 0 Step -1
                If Not IsNothing(_matriz(_mouseX, count)) Then
                    If _matriz(_mouseX, count).ToString().Contains("KingBlack") And _matriz(_mouseX, _mouseY).ToString().Contains("White") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    ElseIf _matriz(_mouseX, count).ToString().Contains("KingWhite") And _matriz(_mouseX, _mouseY).ToString().Contains("Black") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                End If
            Next
            For count As Integer = _mouseY + 1 To 7
                If Not IsNothing(_matriz(_mouseX, count)) Then
                    If _matriz(_mouseX, count).ToString().Contains("KingBlack") And _matriz(_mouseX, _mouseY).ToString().Contains("White") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    ElseIf _matriz(_mouseX, count).ToString().Contains("KingWhite") And _matriz(_mouseX, _mouseY).ToString().Contains("Black") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                End If
            Next
            For count As Integer = _mouseX - 1 To 0 Step -1
                If Not IsNothing(_matriz(count, _mouseY)) Then
                    If _matriz(_mouseX, count).ToString().Contains("KingBlack") And _matriz(_mouseX, _mouseY).ToString().Contains("White") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    ElseIf _matriz(_mouseX, count).ToString().Contains("KingWhite") And _matriz(_mouseX, _mouseY).ToString().Contains("Black") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                End If
            Next
            For count As Integer = _mouseX + 1 To 7
                If Not IsNothing(_matriz(count, _mouseY)) Then
                    If _matriz(_mouseX, count).ToString().Contains("Ajedrez_2015029.KingBlack") And _matriz(_mouseX, _mouseY).ToString().Contains("White") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    ElseIf _matriz(_mouseX, count).ToString().Contains("Ajedrez_2015029.KingWhite") And _matriz(_mouseX, _mouseY).ToString().Contains("Black") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                End If
            Next
        ElseIf _matriz(_mouseX, _mouseY).ToString.Contains("Bishop") Or _matriz(_mouseX, _mouseY).ToString.Contains("Queen") Then
            _val = _mouseY
                For count As Integer = _mouseX + 1 To 7
                    _val -= 1
                    If Not IsNothing(_matriz(count, _val)) Then
                    If _matriz(count, _val).ToString.Equals("Ajedrez_2015029.KingBlack") And _matriz(_mouseX, _mouseY).ToString.Contains("White") Then
                        MsgBox("jaque mate con tomate")
                        Exit For
                    ElseIf _matriz(count, _val).ToString.Equals("Ajedrez_2015029.KingWhite") And _matriz(_mouseX, _mouseY).ToString.Contains("Black") Then
                        MsgBox("Jaque mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                    End If
                Next

            _val = _mouseY
            For count As Integer = _mouseX + 1 To 7
                _val = _val + 1
                If Not IsNothing(_matriz(count, _val)) Then
                    If _matriz(count, _val).ToString.Equals("Ajedrez_2015029.KingBlack") And _matriz(_mouseX, _mouseY).ToString.Contains("White") Then
                        MsgBox("Jaque Mate con tomate")
                        Exit For
                    ElseIf _matriz(count, _val).ToString.Equals("Ajedrez_2015029.KingWhite") And _matriz(_mouseX, _mouseY).ToString.Contains("Black") Then
                        MsgBox("Jaque mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                End If
            Next

            _val = _mouseY
            For count As Integer = _newX - 1 To _mouseX Step -1
                    _val = _val - 1
                    If Not IsNothing(_matriz(count, _val)) Then
                    If _matriz(count, _val).ToString.Equals("Ajedrez_2015029.KingBlack") And _matriz(_mouseX, _mouseY).ToString.Contains("White") Then
                        MsgBox("Jaque mate con tomate")
                        Exit For
                    ElseIf _matriz(count, _val).ToString.Equals("Ajedrez_2015029.KingWhite") And _matriz(_mouseX, _mouseY).ToString.Contains("Black") Then
                        MsgBox("Jaque mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                    End If
                Next
            _val = _mouseY
            For count As Integer = _newX - 1 To _mouseX Step -1
                _val = _val + 1
                If Not IsNothing(_matriz(count, _val)) Then
                    If _matriz(count, _val).ToString.Contains("Ajedrez_2015029.KingBlack") And _matriz(_mouseX, _mouseY).ToString.Contains("White") Then
                        MsgBox("Jaque mate con tomate")
                        Exit For
                    ElseIf _matriz(count, _val).ToString.Contains("Ajedrez_2015029.KingWhite") And _matriz(_mouseX, _mouseY).ToString.Contains("Black") Then
                        MsgBox("jaque mate con tomate")
                        Exit For
                    Else
                        Exit For
                    End If
                End If
            Next
        End If


    End Sub

    Private Sub Tablero_MouseDown(sender As Object, e As MouseButtonEventArgs)
        If _alternador Then

            Try
                If IsNothing(objeto) Then
                    _value = _matriz(_mouseX, _mouseY).ToString
                    If _value.Contains("White") Then
                        addSelection()
                        objeto = _matriz(_mouseX, _mouseY)
                        _newX = _mouseX
                        _newY = _mouseY
                    End If

                ElseIf Not IsNothing(objeto) Then
                    If objeto.ToString = "Ajedrez_2015029.PawnWhite" Then
                        If (_mouseX = _newX) Then
                            objetoUp = _matriz(_mouseX, _mouseY)
                            If (_mouseY = _newY) Then
                                objeto = Nothing
                                Tablero.Children.Remove(_selectionSquare)
                            End If
                            If (_newY = 6) Then
                                If (_mouseY = _newY - 1 Or _mouseY = _newY - 2 And IsNothing(objetoUp) And IsNothing(_matriz(_mouseX, _newY - 1))) Then
                                    Clear()
                                End If
                            Else
                                If (_mouseY = _newY - 1 And IsNothing(objetoUp)) Then
                                    Clear()
                                End If
                            End If
                        ElseIf (_mouseX = _newX + 1 Or _mouseX = _newX - 1) Then
                            objetoUp = _matriz(_mouseX, _mouseY)
                            If (_mouseY = _newY - 1 And Not IsNothing(objetoUp) And objetoUp.ToString.Contains("Black")) Then
                                Eat()
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                            End If
                        Else
                            Tablero.Children.Remove(_selectionSquare)
                            objeto = Nothing
                        End If

                    ElseIf objeto.ToString.Contains("RookWhite") Then
                        RookMovement()

                    ElseIf objeto.ToString.Contains("BishopWhite") Then
                        BishopMovement()

                    ElseIf objeto.ToString.Contains("KnightWhite") Then
                        KnightMovement()

                    ElseIf objeto.ToString.Contains("KingWhite") Then
                        KingMovement()
                    ElseIf objeto.ToString.Contains("QueenWhite") Then
                        QueenMovement()
                    End If
                End If

            Catch ex As Exception

            End Try
        Else
            Try
                If IsNothing(objeto) Then
                    _value = _matriz(_mouseX, _mouseY).ToString
                    If _value.Contains("Black") Then
                        addSelection()
                        objeto = _matriz(_mouseX, _mouseY)
                        _newX = _mouseX
                        _newY = _mouseY
                    End If

                ElseIf Not IsNothing(objeto) Then
                    If objeto.ToString = "Ajedrez_2015029.PawnBlack" Then
                        If (_mouseX = _newX) Then
                            objetoUp = _matriz(_mouseX, _mouseY)
                            If (_mouseY = _newY) Then
                                objeto = Nothing
                                Tablero.Children.Remove(_selectionSquare)
                            End If
                            If (_newY = 1) Then
                                If (_mouseY = _newY + 1 Or _mouseY = _newY + 2 And IsNothing(objetoUp) And IsNothing(_matriz(_mouseX, _newY + 1))) Then
                                    Clear()
                                End If
                            Else
                                If (_mouseY = _newY + 1 And IsNothing(objetoUp)) Then
                                    Clear()
                                End If
                            End If
                        ElseIf (_mouseX = _newX + 1 Or _mouseX = _newX - 1) Then
                            objetoUp = _matriz(_mouseX, _mouseY)
                            If (_mouseY = _newY + 1 And Not IsNothing(objetoUp) And objetoUp.ToString.Contains("White")) Then
                                Eat()
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                            End If
                        Else
                            Tablero.Children.Remove(_selectionSquare)
                            objeto = Nothing
                        End If

                    ElseIf _matriz(_newX, _newY).ToString = "Ajedrez_2015029.RookBlack" Then
                        RookMovement()

                    ElseIf _matriz(_newX, _newY).ToString = "Ajedrez_2015029.BishopBlack" Then
                        BishopMovement()

                    ElseIf _matriz(_newX, _newY).ToString = "Ajedrez_2015029.BlackKnight" Then
                        KnightMovement()

                    ElseIf _matriz(_newX, _newY).ToString = "Ajedrez_2015029.KingBlack" Then
                        KingMovement()
                    ElseIf _matriz(_newX, _newY).ToString = "Ajedrez_2015029.QueenBlack" Then
                        QueenMovement()

                    Else
                        Clear()
                    End If

                End If
            Catch ex As Exception

            End Try
        End If

    End Sub

    Sub Clear()

        Tablero.Children.Remove(_selectionSquare)
        _valid = True
        Tablero.Children.Remove(_matriz(_newX, _newY))
        _matriz(_newX, _newY) = Nothing
        Grid.SetColumn(objeto, _mouseX)
        Grid.SetRow(objeto, _mouseY)
        Tablero.Children.Add(objeto)
        _matriz(_mouseX, _mouseY) = objeto
        objeto = Nothing
        If _alternador Then
            _alternador = False
            _turno = "Negras"
        Else
            _alternador = True
            _turno = "Blancas"
        End If
        Jaque()
    End Sub

    Sub Eat()
        Tablero.Children.Remove(_matriz(_mouseX, _mouseY))
        _matriz(_mouseX, _mouseY) = Nothing
        Clear()
        objeto = Nothing
    End Sub

    Sub RookMovement()
        _valid = True
        If _mouseX = _newX And _mouseY = _newY Then
            objeto = Nothing
            _valid = False
            Tablero.Children.Remove(_selectionSquare)
        Else
            If _mouseX <> _newX And _mouseY = _newY Then
                If _mouseX > _newX Then
                    For count As Integer = _newX + 1 To _mouseX
                        If Not IsNothing(_matriz(count, _newY)) Then
                            If _matriz(count, _newY).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _newY).ToString.Contains("White") And objeto.ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next

                ElseIf _mouseX < _newX Then
                    For count As Integer = _newX - 1 To _mouseX Step -1
                        If Not IsNothing(_matriz(count, _newY)) Then
                            If _matriz(count, _newY).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _newY).ToString.Contains("White") And objeto.ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                Else
                    Tablero.Children.Remove(_selectionSquare)
                    _valid = False
                    objeto = Nothing
                End If
            ElseIf _mouseX = _newX And _mouseY <> _newY Then
                If _newY > _mouseY Then
                    For count As Integer = _newY - 1 To _mouseY Step -1
                        If Not IsNothing(_matriz(_newX, count)) Then
                            If _matriz(_newX, count).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(_newX, count).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                ElseIf _newY < _mouseY Then
                    For count As Integer = _newY + 1 To _mouseY Step 1
                        If Not IsNothing(_matriz(_newX, count)) Then
                            If _matriz(_newX, count).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(_newX, count).ToString.Contains("White") And objeto.ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                Else
                    Tablero.Children.Remove(_selectionSquare)
                    objeto = Nothing
                    _valid = False
                End If
            Else
                Tablero.Children.Remove(_selectionSquare)
                objeto = Nothing
                _valid = False
            End If
        End If

        If _valid Then
            If _newX = 7 And _newY = 7 Then
                _pawnWhiteR = False
            ElseIf _newX = 0 And _newY = 7 Then
                _pawnWhiteL = False
            ElseIf _newX = 7 And _newY = 0 Then
                _pawnBlackR = False
            ElseIf _newX = 0 And _newY = 0 Then
                _pawnBlackL = False
            End If
            Clear()
        End If

    End Sub

    Dim _val As Integer

    Sub BishopMovement()
        _valid = True
        If _mouseX = _newX And _mouseY = _newY Then
            objeto = Nothing
            _valid = False
            Tablero.Children.Remove(_selectionSquare)
        Else
            If Math.Abs(_newX - _mouseX) = Math.Abs(_newY - _mouseY) Then
                If (_mouseX > _newX And _mouseY < _newY) Then
                    _val = _newY
                    For count As Integer = _newX + 1 To _mouseX
                        _val -= 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                _valid = False
                                objeto = Nothing
                            End If
                        End If
                    Next
                ElseIf (_mouseX > _newX And _mouseY > _newY) Then
                    _val = _newY
                    For count As Integer = _newX + 1 To _mouseX
                        _val = _val + 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                _valid = False
                                objeto = Nothing
                            End If
                        End If
                    Next
                ElseIf (_mouseX < _newX And _mouseY < _newY) Then
                    _val = _newY
                    For count As Integer = _newX - 1 To _mouseX Step -1
                        _val = _val - 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                _valid = False
                                objeto = Nothing
                            End If
                        End If
                    Next
                ElseIf (_mouseX < _newX And _mouseY > _newY) Then
                    _val = _newY
                    For count As Integer = _newX - 1 To _mouseX Step -1
                        _val = _val + 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                _valid = False
                                objeto = Nothing
                            End If
                        End If
                    Next
                Else
                    Tablero.Children.Remove(_selectionSquare)
                    objeto = Nothing
                    _valid = False
                End If
            Else
                Tablero.Children.Remove(_selectionSquare)
                objeto = Nothing
                _valid = False
            End If
        End If
        If _valid Then
            Clear()

        End If
    End Sub

    Sub KnightMovement()
        _valid = True
        If _mouseX = _newX And _mouseY = _newY Then
            objeto = Nothing
            _valid = False
            Tablero.Children.Remove(_selectionSquare)
        Else
            If (_mouseX = _newX + 1 And _mouseY = _newY - 2) Or (_mouseX = _newX - 1 And _mouseY = _newY - 2) Or
               (_mouseX = _newX - 2 And _mouseY = _newY - 1) Or (_mouseX = _newX - 2 And _mouseY = _newY + 1) Or
               (_mouseX = _newX + 2 And _mouseY = _newY - 1) Or (_mouseX = _newX + 2 And _mouseY = _newY + 1) Or
               (_mouseX = _newX + 1 And _mouseY = _newY + 2) Or (_mouseX = _newX - 1 And _mouseY = _newY + 2) Then
                If Not IsNothing(_matriz(_mouseX, _mouseY)) Then
                    If _matriz(_mouseX, _mouseY).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                        Eat()
                        _valid = False
                    ElseIf _matriz(_mouseX, _mouseY).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                        Eat()
                        _valid = False
                    Else
                        Tablero.Children.Remove(_selectionSquare)
                        objeto = Nothing
                        _valid = False
                    End If
                End If
            Else
                Tablero.Children.Remove(_selectionSquare)
                _valid = False
                objeto = Nothing
            End If
        End If
        If _valid Then
            Clear()
        End If
    End Sub


    Public Sub Rook(ByVal oldX As Integer, ByVal oldY As Integer, ByVal newX As Integer, ByVal newY As Integer)
        Dim castling As Object = _matriz(oldX, oldY)
        Grid.SetColumn(castling, oldX)
        Grid.SetRow(castling, oldY)
        Tablero.Children.Remove(_matriz(oldX, oldY))
        _matriz(oldX, oldY) = Nothing


        Grid.SetColumn(castling, newX)
        Grid.SetRow(castling, newY)
        Tablero.Children.Add(castling)
        _matriz(newX, newY) = castling
    End Sub

    Sub KingMovement()
        _valid = True
        If _mouseX = _newX And _mouseY = _newY Then
            objeto = Nothing
            _valid = False
            Tablero.Children.Remove(_selectionSquare)
        ElseIf _mouseX = _newX + 2 And _newY = 7 And _pawnWhiteR And _KingWhite Then
            If IsNothing(_matriz(_newX + 1, 7)) And IsNothing(_matriz(_newX + 2, 7)) Then

                Clear()
                Rook(7, 7, _mouseX - 1, _mouseY)
                _pawnWhiteR = False
                _KingWhite = False
                Tablero.Children.Remove(_selectionSquare)
            End If
            _valid = False
            objeto = Nothing
            Tablero.Children.Remove(_selectionSquare)
        ElseIf _mouseX = _newX - 2 And _newY = 7 And _pawnWhiteL And _KingWhite Then
            If IsNothing(_matriz(_newX - 1, _newY)) And IsNothing(_matriz(_newX - 2, _newY)) Then

                Clear()
                Rook(0, 7, _mouseX + 1, _mouseY)
                _pawnWhiteL = False
                _KingWhite = False
                Tablero.Children.Remove(_selectionSquare)
            End If
            _valid = False
            objeto = Nothing
            Tablero.Children.Remove(_selectionSquare)
        ElseIf _mouseX = _newX - 2 And _newY = 0 And _pawnBlackL And _KingBlack Then
            If IsNothing(_matriz(_newX - 1, _newY)) And IsNothing(_matriz(_newX - 2, _newY)) Then

                Clear()
                Rook(0, 0, _mouseX + 1, _mouseY)
                _pawnBlackL = False
                _KingBlack = False
                Tablero.Children.Remove(_selectionSquare)
            End If
            _valid = False
            objeto = Nothing
            Tablero.Children.Remove(_selectionSquare)
        ElseIf _mouseX = _newX + 2 And _newY = 0 And _pawnBlackR And _KingBlack Then
            If IsNothing(_matriz(_newX + 1, 0)) And IsNothing(_matriz(_newX + 2, 0)) Then

                Clear()
                Rook(7, 0, _mouseX - 1, _mouseY)
                _pawnBlackR = False
                _KingBlack = False
                Tablero.Children.Remove(_selectionSquare)
            End If
            Tablero.Children.Remove(_selectionSquare)
            _valid = False
            objeto = Nothing
            Tablero.Children.Remove(_selectionSquare)
        ElseIf _mouseX <> _newX Or _mouseY <> _newY Then
            If (_mouseX = _newX + 1 And _mouseY = _newY - 1) Or (_mouseX = _newX + 1 And _mouseY = _newY + 1) Or
           (_mouseX = _newX - 1 And _mouseY = _newY - 1) Or (_mouseX = _newX - 1 And _mouseY = _newY + 1) Or
           (_mouseX = _newX And _mouseY = _newY - 1) Or (_mouseX = _newX And _mouseY = _newY + 1) Or
           (_mouseX = _newX - 1 And _mouseY = _newY) Or (_mouseX = _newX + 1 And _mouseY = _newY) Then
                If Not IsNothing(_matriz(_mouseX, _mouseY)) Then
                    If _matriz(_mouseX, _mouseY).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                        Eat()
                        _KingWhite = False
                        _valid = False

                    ElseIf _matriz(_mouseX, _mouseY).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                        Eat()
                        _KingWhite = False
                        _valid = False
                    Else
                        Tablero.Children.Remove(_selectionSquare)
                        objeto = Nothing
                        _valid = False
                    End If
                End If
            Else
                Tablero.Children.Remove(_selectionSquare)
                _valid = False
                objeto = Nothing
            End If
        Else
            Tablero.Children.Remove(_selectionSquare)
            _valid = False
            objeto = Nothing
        End If
        If _valid Then
            Clear()
            _KingWhite = False
        End If
    End Sub

    Sub QueenMovement()
        _valid = True
        Dim papu As Boolean = True
        If _mouseX = _newX And _mouseY = _newY Then
            objeto = Nothing
            _valid = False
            Tablero.Children.Remove(_selectionSquare)
        Else
            If _mouseX <> _newX And _mouseY = _newY Then
                If _mouseX > _newX Then
                    For count As Integer = _newX + 1 To _mouseX
                        If Not IsNothing(_matriz(count, _newY)) Then
                            If _matriz(count, _newY).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _newY).ToString.Contains("White") And objeto.ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next

                ElseIf _mouseX < _newX Then
                    For count As Integer = _newX - 1 To _mouseX Step -1
                        If Not IsNothing(_matriz(count, _newY)) Then
                            If _matriz(count, _newY).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _newY).ToString.Contains("White") And objeto.ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                Else
                    Tablero.Children.Remove(_selectionSquare)
                    _valid = False
                    objeto = Nothing
                End If
                papu = False
            ElseIf _mouseX = _newX And _mouseY <> _newY Then
                If _newY > _mouseY Then
                    For count As Integer = _newY - 1 To _mouseY Step -1
                        If Not IsNothing(_matriz(_newX, count)) Then
                            If _matriz(_newX, count).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(_newX, count).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                ElseIf _newY < _mouseY Then
                    For count As Integer = _newY + 1 To _mouseY Step 1
                        If Not IsNothing(_matriz(_newX, count)) Then
                            If _matriz(_newX, count).ToString.Contains("Black") And objeto.ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(_newX, count).ToString.Contains("White") And objeto.ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                Else
                    Tablero.Children.Remove(_selectionSquare)
                    objeto = Nothing
                    _valid = False
                End If
                papu = False
            ElseIf Math.Abs(_newX - _mouseX) = Math.Abs(_newY - _mouseY) And papu Then
                If (_mouseX > _newX And _mouseY < _newY) Then
                    _val = _newY
                    For count As Integer = _newX + 1 To _mouseX
                        _val -= 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                _valid = False
                                objeto = Nothing
                            End If
                        End If
                    Next
                ElseIf (_mouseX > _newX And _mouseY > _newY) Then
                    _val = _newY
                    For count As Integer = _newX + 1 To _mouseX
                        _val = _val + 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                _valid = False
                                objeto = Nothing
                            End If
                        End If
                    Next
                ElseIf (_mouseX < _newX And _mouseY < _newY) Then
                    _val = _newY
                    For count As Integer = _newX - 1 To _mouseX Step -1
                        _val = _val - 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                ElseIf (_mouseX < _newX And _mouseY > _newY) Then
                    _val = _newY
                    For count As Integer = _newX - 1 To _mouseX Step -1
                        _val = _val + 1
                        If Not IsNothing(_matriz(count, _val)) Then
                            If _matriz(count, _val).ToString.Contains("Black") And _matriz(_newX, _newY).ToString.Contains("White") Then
                                Eat()
                                _valid = False
                            ElseIf _matriz(count, _val).ToString.Contains("White") And _matriz(_newX, _newY).ToString.Contains("Black") Then
                                Eat()
                                _valid = False
                            Else
                                Tablero.Children.Remove(_selectionSquare)
                                objeto = Nothing
                                _valid = False
                            End If
                        End If
                    Next
                Else
                    Tablero.Children.Remove(_selectionSquare)
                    objeto = Nothing
                    _valid = False
                End If
            Else
                Tablero.Children.Remove(_selectionSquare)
                objeto = Nothing
                _valid = False
            End If
        End If

        If _valid Then
            Clear()
        End If
    End Sub

    Dim messageBoxText As String
    Dim caption As String = "Word Processor"
    Dim button As MessageBoxButton = MessageBoxButton.YesNo
    Dim icon As MessageBoxImage = MessageBoxImage.Question

    Private Sub MenuItem_Click(sender As Object, e As RoutedEventArgs)
        messageBoxText = "¿Desea iniciar una nueva partida?"
        Dim result As MessageBoxResult = MessageBox.Show(messageBoxText, caption, button, icon)
        Select Case result
            Case MessageBoxResult.Yes
                Tablero.Children.Clear()
                For count As Integer = 0 To 7
                    For countY As Integer = 0 To 7
                        _matriz(count, countY) = Nothing
                    Next
                Next
                RestarPieces()
                _turno = "Blancas"
                _alternador = True
        End Select
    End Sub


    Private Sub MenuItem_Click_1(sender As Object, e As RoutedEventArgs)
        messageBoxText = "¿Seguro que desea salir?"
        Dim result As MessageBoxResult = MessageBox.Show(messageBoxText, caption, button, icon)

        Select Case result
            Case MessageBoxResult.Yes
                Close()
        End Select
    End Sub

    Private Sub Window_KeyDown(sender As Object, e As KeyEventArgs)
        If e.Key = Key.F2 Then
            messageBoxText = "¿Desea iniciar una nueva partida?"
            Dim result As MessageBoxResult = MessageBox.Show(messageBoxText, caption, button, icon)
            Select Case result
                Case MessageBoxResult.Yes
                    Tablero.Children.Clear()
                    For count As Integer = 0 To 7
                        For countY As Integer = 0 To 7
                            _matriz(count, countY) = Nothing
                        Next
                    Next
                    RestarPieces()
                    _turno = "Blancas"
                    _alternador = True
                    _pawnBlackL = True
                    _pawnBlackR = True
                    _pawnWhiteL = True
                    _pawnWhiteR = True
                    _KingBlack = True
                    _KingWhite = True
            End Select
        ElseIf e.Key = Key.Escape Then

            messageBoxText = "¿Seguro que desea salir?"
            Dim result As MessageBoxResult = MessageBox.Show(messageBoxText, caption, button, icon)
            Select Case result
                Case MessageBoxResult.Yes
                    Close()
            End Select
        End If
    End Sub
End Class