﻿Public Class Coronation

    Private _matriz(0 To 7, 0 To 7) As Object

    Public Sub New()

        InitializeComponent()

        Dim blackColor As New SolidColorBrush(Color.FromRgb(222, 227, 231))
        Dim whiteColor As New SolidColorBrush(Color.FromRgb(95, 107, 114))

        For rower As Integer = 0 To 1

            For columne As Integer = 0 To 1
                If columne Mod 2 = 0 Then
                    Dim whiteSquare As New Rectangle
                    whiteSquare.Fill = whiteColor
                    If (rower Mod 2) Then
                        Grid.SetRow(whiteSquare, rower)
                        Grid.SetColumn(whiteSquare, columne)
                    Else
                        Grid.SetRow(whiteSquare, rower)
                        Grid.SetColumn(whiteSquare, columne + 1)
                    End If
                    Coron.Children.Add(whiteSquare)
                Else
                    Dim blackSquare2 As New Rectangle
                    blackSquare2.Fill = blackColor
                    If (rower Mod 2) Then
                        Grid.SetRow(blackSquare2, rower)
                        Grid.SetColumn(blackSquare2, columne)
                    Else
                        Grid.SetRow(blackSquare2, rower)
                        Grid.SetColumn(blackSquare2, columne - 1)
                    End If
                    Coron.Children.Add(blackSquare2)

                End If
            Next
        Next
        RestrarPieces()

    End Sub

    Public Sub RestrarPieces()

        Dim _whiteCircle As New QueenWhite
        Grid.SetColumn(_whiteCircle, 0)
        Grid.SetRow(_whiteCircle, 0)
        Coron.Children.Add(_whiteCircle)
        _matriz(0, 0) = _whiteCircle

        Dim _whiteCircle1 As New BishopWhite
        Grid.SetColumn(_whiteCircle1, 1)
        Grid.SetRow(_whiteCircle1, 0)
        Coron.Children.Add(_whiteCircle1)
        _matriz(1, 0) = _whiteCircle1

        Dim _whiteCircle2 As New KnightWhite
        Grid.SetColumn(_whiteCircle2, 0)
        Grid.SetRow(_whiteCircle2, 1)
        Coron.Children.Add(_whiteCircle2)
        _matriz(0, 1) = _whiteCircle2

        Dim _whiteCircle3 As New RookWhite
        Grid.SetColumn(_whiteCircle3, 1)
        Grid.SetRow(_whiteCircle3, 1)
        Coron.Children.Add(_whiteCircle3)
        _matriz(1, 1) = _whiteCircle3

    End Sub

    Dim _x As Integer
    Dim _Y As Integer

    Dim objeto As Object

    Private Sub Coron_MouseDown(sender As Object, e As MouseButtonEventArgs)
        Dim newWin As New MainWindow
        newWin.Close()
        objeto = _matriz(_x, _Y)
    End Sub

    Private Sub Coron_MouseMove_1(sender As Object, e As MouseEventArgs)
        _x = e.GetPosition(sender).X \ 131
        _Y = e.GetPosition(sender).Y \ 131
    End Sub

End Class
